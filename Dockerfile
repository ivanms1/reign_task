FROM node:lts-slim

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

EXPOSE 4000

RUN 'yarn'

CMD [ "yarn", "run", "start" ]