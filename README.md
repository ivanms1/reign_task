## HackerNews Node.js Newsfeed

To run the project make sure to have `docker` and `docker-compose` installed.

- To start the project run in the project's root directory `docker-compose up`

- App should be served in `http://localhost:3000/`
