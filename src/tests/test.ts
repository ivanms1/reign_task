process.env.NODE_ENV = 'test';

import 'mocha';
import chaiHttp from 'chai-http';
import chai from 'chai';

import app from '../app';
import { apiCall } from '../apiCall';
import Article from '../article/model';

chai.use(chaiHttp);

describe('Article Tests', () => {
  before(async () => {
    await apiCall();
  });

  after(async () => {
    await Article.deleteMany({});
  });
  it('should return articles', done => {
    chai
      .request(app)
      .get('/articles')
      .end((err, res) => {
        chai.expect(res.status).to.equal(200);
        chai.expect(res.body).to.be.a('array');
        done();
      });
  });
  it('should remove an article', done => {
    chai
      .request(app)
      .get('/articles')
      .end((err, res) => {
        chai.expect(res.status).to.equal(200);
        chai.expect(res.body).to.be.a('array');
        const articleToRemove = res.body[0];
        chai
          .request(app)
          .delete(`/${articleToRemove._id}`)
          .end(async (err, res) => {
            chai.expect(res.status).to.equal(200);
            chai.expect(res.body).to.equal(articleToRemove._id);
            const removedArticle = await Article.findById(articleToRemove);
            chai.expect(removedArticle.isRemoved).to.equal(true);
            done();
          });
        done();
      });
  });
});
