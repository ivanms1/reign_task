import { Schema, model, Document } from 'mongoose';

export interface IArticle extends Document {
  _id: number;
  created_at: string;
  title: string;
  url: string;
  author: string;
  isRemoved: boolean;
}

const ArticleSchema: Schema = new Schema(
  {
    _id: {
      type: Number,
      required: true
    },
    created_at: {
      type: Date,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    url: {
      type: String,
      required: true
    },
    author: {
      type: String,
      required: true
    },
    isRemoved: {
      type: Boolean,
      default: false
    }
  },
  { _id: false }
);

const Article = model<IArticle>('article', ArticleSchema);

export default Article;
