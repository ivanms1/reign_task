import fetch from 'node-fetch';
import Article from './article/model';

export const apiCall = async () => {
  const response = await fetch(
    'https://hn.algolia.com/api/v1/search_by_date?query=node,js&tags=story'
  );
  const data: any = await response.json();
  data.hits.forEach(async (hit: any) => {
    const {
      story_title,
      title,
      objectID,
      created_at,
      url,
      story_url,
      author
    } = hit;
    if ((!story_title && !title) || (!story_url && !url)) {
    } else {
      await Article.findById(objectID).then(article => {
        if (article) {
          console.log('Article already added');
        } else {
          new Article({
            _id: objectID,
            created_at,
            title: story_title || title,
            url: story_url || url,
            author
          }).save();
          console.log('Article added');
        }
      });
    }
  });
};
