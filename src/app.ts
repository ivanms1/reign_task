import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import { CronJob } from 'cron';

import { apiCall } from './apiCall';
import Article from './article/model';

const app = express();
const port = 4000;

const DB_URI =
  process.env.NODE_ENV === 'test'
    ? 'mongodb://mongo:27017/hackernewsTest'
    : 'mongodb://mongo:27017/hackernews';

mongoose
  .connect(DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => console.log('Connected to database'))
  .catch(err => console.log(err));

app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true
  })
);

const job = new CronJob(
  '0 * * * *',
  apiCall,
  null,
  true,
  'America/Los_Angeles'
);

if (process.env.NODE_ENV !== 'test') {
  apiCall();
  job.start();
}

app.get('/articles', async (req, res) => {
  const articles = await Article.find({ isRemoved: false });
  res.send(articles);
});

app.delete('/:articleId', async (req, res) => {
  const articleToRemove = await Article.findByIdAndUpdate(
    req.params.articleId,
    {
      isRemoved: true
    }
  );
  res.json(articleToRemove._id);
});

app.listen(port, err => {
  if (err) {
    return console.error(err);
  }
  return console.log(`Server is listening on ${port}`);
});

export default app;
