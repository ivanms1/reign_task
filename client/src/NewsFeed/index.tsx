import React from 'react';
import formatRelative from 'date-fns/formatRelative';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography';

import styles from './NewsFeed.module.css';

interface NewsFeedProps {
  articles: {
    _id: number;
    created_at: string;
    title: string;
    author: string;
    url: string;
  }[];
  removeArticle: (articleId: number) => void;
}

const NewsFeed = ({ articles, removeArticle }: NewsFeedProps) => {
  const onRemove = (articleId: number) => {
    fetch(`/${articleId}`, { method: 'DELETE' }).then(res =>
      removeArticle(articleId)
    );
  };
  return (
    <List>
      {articles.map(article => (
        <React.Fragment key={article._id}>
          <ListItem
            component='a'
            className={styles.ListItem}
            href={article.url}
            target='_blank'
          >
            <ListItemText
              className={styles.ListItemText}
              primary={article.title}
              secondary={article.author}
            />
            <Typography variant='body1'>
              {formatRelative(new Date(article.created_at), new Date())}
            </Typography>

            <ListItemSecondaryAction>
              <IconButton
                edge='end'
                aria-label='comments'
                onClick={() => onRemove(article._id)}
              >
                <DeleteIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
          <Divider />
        </React.Fragment>
      ))}
    </List>
  );
};

export default NewsFeed;
