import React, { useEffect, useState } from 'react';

import NewsFeed from './NewsFeed';

import './App.css';
import 'typeface-roboto';

function App() {
  const [articles, setArticles] = useState<
    {
      _id: number;
      created_at: string;
      title: string;
      author: string;
      url: string;
      isRemoved: boolean;
    }[]
  >([]);
  useEffect(() => {
    fetch('http://localhost:4000/articles')
      .then(response => response.json())
      .then(data => setArticles(data));
  }, []);

  const removeArticle = (articleId: number) => {
    setArticles(articles.filter(article => article._id !== articleId));
  };

  const sortedArticles = articles
    // @ts-ignore
    .sort((a, b) => new Date(b.created_at) - new Date(a.created_at));

  return (
    <div>
      <div>
        <div className='header'>
          <h1>HN Feed</h1>
          <h3>Node.js</h3>
        </div>
        <NewsFeed articles={sortedArticles} removeArticle={removeArticle} />
      </div>
    </div>
  );
}

export default App;
