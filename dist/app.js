"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const cron_1 = require("cron");
const apiCall_1 = require("./apiCall");
const model_1 = __importDefault(require("./article/model"));
const app = express_1.default();
const port = 4000;
const DB_URI = process.env.NODE_ENV === 'test'
    ? 'mongodb://mongo:27017/hackernewsTest'
    : 'mongodb://mongo:27017/hackernews';
mongoose_1.default
    .connect(DB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    .then(() => console.log('Connected to database'))
    .catch(err => console.log(err));
app.use(cors_1.default({
    origin: 'http://localhost:3000',
    credentials: true
}));
const job = new cron_1.CronJob('0 * * * *', apiCall_1.apiCall, null, true, 'America/Los_Angeles');
if (process.env.NODE_ENV !== 'test') {
    apiCall_1.apiCall();
    job.start();
}
app.get('/articles', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const articles = yield model_1.default.find({ isRemoved: false });
    res.send(articles);
}));
app.delete('/:articleId', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const articleToRemove = yield model_1.default.findByIdAndUpdate(req.params.articleId, {
        isRemoved: true
    });
    res.json(articleToRemove._id);
}));
app.listen(port, err => {
    if (err) {
        return console.error(err);
    }
    return console.log(`Server is listening on ${port}`);
});
exports.default = app;
//# sourceMappingURL=app.js.map