"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ArticleSchema = new mongoose_1.Schema({
    _id: {
        type: Number,
        required: true
    },
    created_at: {
        type: Date,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    isRemoved: {
        type: Boolean,
        default: false
    }
}, { _id: false });
const Article = mongoose_1.model('article', ArticleSchema);
exports.default = Article;
//# sourceMappingURL=model.js.map