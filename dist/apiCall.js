"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_fetch_1 = __importDefault(require("node-fetch"));
const model_1 = __importDefault(require("./article/model"));
exports.apiCall = () => __awaiter(void 0, void 0, void 0, function* () {
    const response = yield node_fetch_1.default('https://hn.algolia.com/api/v1/search_by_date?query=node,js&tags=story');
    const data = yield response.json();
    data.hits.forEach((hit) => __awaiter(void 0, void 0, void 0, function* () {
        const { story_title, title, objectID, created_at, url, story_url, author } = hit;
        if ((!story_title && !title) || (!story_url && !url)) {
        }
        else {
            yield model_1.default.findById(objectID).then(article => {
                if (article) {
                    console.log('Article already added');
                }
                else {
                    new model_1.default({
                        _id: objectID,
                        created_at,
                        title: story_title || title,
                        url: story_url || url,
                        author
                    }).save();
                    console.log('Article added');
                }
            });
        }
    }));
});
//# sourceMappingURL=apiCall.js.map