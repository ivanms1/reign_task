"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
process.env.NODE_ENV = 'test';
require("mocha");
const chai_http_1 = __importDefault(require("chai-http"));
const chai_1 = __importDefault(require("chai"));
const app_1 = __importDefault(require("../app"));
const apiCall_1 = require("../apiCall");
const model_1 = __importDefault(require("../article/model"));
chai_1.default.use(chai_http_1.default);
describe('Article Tests', () => {
    before(() => __awaiter(void 0, void 0, void 0, function* () {
        yield apiCall_1.apiCall();
    }));
    after(() => __awaiter(void 0, void 0, void 0, function* () {
        yield model_1.default.deleteMany({});
    }));
    it('should return articles', done => {
        chai_1.default
            .request(app_1.default)
            .get('/articles')
            .end((err, res) => {
            chai_1.default.expect(res.status).to.equal(200);
            chai_1.default.expect(res.body).to.be.a('array');
            done();
        });
    });
    it('should remove an article', done => {
        chai_1.default
            .request(app_1.default)
            .get('/articles')
            .end((err, res) => {
            chai_1.default.expect(res.status).to.equal(200);
            chai_1.default.expect(res.body).to.be.a('array');
            const articleToRemove = res.body[0];
            chai_1.default
                .request(app_1.default)
                .delete(`/${articleToRemove._id}`)
                .end((err, res) => __awaiter(void 0, void 0, void 0, function* () {
                chai_1.default.expect(res.status).to.equal(200);
                chai_1.default.expect(res.body).to.equal(articleToRemove._id);
                const removedArticle = yield model_1.default.findById(articleToRemove);
                chai_1.default.expect(removedArticle.isRemoved).to.equal(true);
                done();
            }));
            done();
        });
    });
});
//# sourceMappingURL=test.js.map